# Sticker Mania

## DATABASE
Those instructions assume you have a postgresql data setup somewhere that can be accessed by an url postgre://....
This postgresql database must be accessible using ssl. You can setup one free database from https://www.heroku.com/postgres


## ENVIRONMENT
These instructions work in a AWS cloud9 nodejs environment. Setup instructions could be different in other environments.

First, clone this repo in your Cloud9 IDE (git clone....).

Then copy the file .env.sample to the new file .env (bash command: cp .env.sample .env)
Then edit the file .env to have the development url for the database above.
Note: to edit the file you may have to edit preferences to:
...sh
"projecttree": {
    "@showhidden": true
},
...

## FIRST INIT
Just type:

```sh
./setup.sh
```
This will populate the database with sample data and install the node modules.

## SERVER
Start the server by typing
...sh
npm start
...
You also need to have the client server hosting static files