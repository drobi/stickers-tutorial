/*
    table.increments();
    table.text('email').unique().notNullable();
    table.text('password').notNullable();
    table.datetime('created_at');
    table.boolean('isAdmin').defaultTo(false);
    table.text('firstName').notNull();
    table.text('middleName');
    table.text('lastName').notNull();
*/

exports.seed = (knex, Promise) => {
    return knex.raw('DELETE FROM "user"; ALTER SEQUENCE user_id_seq RESTART WITH 3')
      .then(() => {
        const users = [
          {
            id: 1,
            email: 'user@user.com',
            password: '$2b$10$FFkHZ9S3Oty4n99lLqhek.J7HI7zrudaxLArrUeqKALYxJlG4pSMu', //hash of 123456
            created_at: new Date(),
            firstName: 'Test',
            lastName: 'User'
          },
          {
            id: 2,
            email: 'admin@admin.com',
            password: '$2b$10$xrftgnd1DDcCk9wvgnS5h.MHU0H5hGVtB11/4B/ug0Ls0TvmrOkny', //hash of 123456
            created_at: new Date(),
            firstName: 'Admin',
            lastName: 'Boss',
            isAdmin: true
          }
        ]
        return knex('user').insert(users)
      })
};
