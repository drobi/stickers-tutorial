redirectIfLoggedIn();

$('#signup-form').submit((event) => {
  event.preventDefault();
  const email = $('#email').val();
  const password = $('#password').val();
  const firstName = $('#first-name').val();
  const middleName = $('#middle-name').val();
  const lastName = $('#last-name').val();

  const user = {
    email,
    password,
    firstName,
    middleName,
    lastName
  };

  signup(user)
    .then(applyLogin)
    .catch(error => {
      showError(error.responseJSON.message);
    });
});

function signup(user) {
  return $.post(AUTH_URL + '/signup', user);
}