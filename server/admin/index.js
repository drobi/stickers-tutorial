const express = require('express');
const router = express.Router();
const dbAdmin = require('../db/admin');

// Returns the list of database tables
router.get('/tables', (req, res, next) => {
  dbAdmin
    .listTables()
    .then(list => {
      let tables = [];
      list.rows.forEach(item => {
        // remove knex tables from the list
        //if(item.table_name.indexOf('knex')<0){
          tables.push(item.table_name);
        //}
      });
      res.json({
        tables
      });
    })
    .catch(error => {
      next(new Error(error));
    });
});

// Allows the admin to send raw SQL queries!
router.post('/db_raw', (req, res, next) => {
  //console.log("Sending SQL: " + req.body.raw)
  dbAdmin
    .raw(req.body.raw)
    .then(response => {
      res.json({
        response
      });
    })
    .catch(error => {
      res.json({
        response: error.message
      });
    });
});

// Returns the whole content of a table
router.get('/table/:name', (req, res, next) => {
  dbAdmin
    .getTable(req.params.name)
    .then(rows => {
      res.json({
        rows
      });
    })
    .catch(error => {
      next(new Error(error));
    });
});

module.exports = router;