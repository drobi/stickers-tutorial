const API_URL = "http://18.217.1.63:8080";
const AUTH_URL = API_URL + '/auth';
const ADMIN_URL = API_URL + '/admin';

// allow settings cookies locally
$.ajaxSetup({
  crossDomain: true,
  xhrFields: {
    withCredentials: true
  }
});

function parseQuery() {
  var query = window.location.search;
  return query.substr(1).split('&').reduce((params, keyValue) => {
    const parts = keyValue.split('=');
    params[parts[0]] = parts[1];
    return params;
  }, {});
}

function showError(message){
  const $err = $('#errorMessage');
  $err.text(message);
  $err.show();
}

// redirect if already logged-in
function redirectIfLoggedIn(){
  if(localStorage.user_id){
    window.location = '/dashboard';
  }
}

// saves the login ID and then redirect
function applyLogin(result){
  // save the user_id in local storage (can't read cookie from js for security reasons)
  localStorage.user_id = result.id;

  redirectIfLoggedIn();
}

function logout(){
  delete localStorage.user_id;

  // get the server's confirmation that logout worked
  $.get(AUTH_URL + '/logout')
    .then(result => {
      window.location = '/';
    });
}