const express = require('express');
const router = express.Router();
const DBitem = require('../db/item');
const DBpermissions = require('../db/permissions');

//Get public items
router.get('/:item/public', (req,res,next)=>{
  DBitem
    .getAll(req.params.item)
    .then(fillCommon)
    .then(filterItemsPublic)
    .then(cleanupResponse)
    .then(items => {
      res.json(items);
    })
    .catch(error => {
      next(new Error(error));
    });
});

//Get logged-in user items
router.get('/:item/user', (req,res,next)=>{
  DBitem
    .getAll(req.params.item)
    .then(fillCommon)
    .then(items => {
      return filterItemsUser(items, req);
    })
    .then(cleanupResponse)
    .then(items => {
      res.json(items);
    })
    .catch(error => {
      next(new Error(error));
    });
});

// Cleanup response JSON to remove unnecessary stuff
function cleanupResponse(items){
  items.forEach(item => {
    delete item.item_common_id;
    delete item.created_at;
    delete item.common.id;
    delete item.common.created_at;
    delete item.common.permissions_id;
  });
  return items;
}

// Fills in the common array
function fillCommon(items){
  return DBitem
    .getAll('common').then(commons => {
      items.forEach(item => {
        item.common = commons[item.item_common_id-1];
      });
      return items;
    });
}

// Remove each item in the array that is not the user's property
function filterItemsUser(items, req){
  console.log("ITEMS", items);
  return DBpermissions
    .getAll().then(permissions => {
      var filtered = [];
      items.forEach(item => {
        const item_permissions = permissions[item.common.permissions_id-1];
        if(item_permissions.owner_id == req.signedCookies.user_id){
          filtered.push(item);
        }
      });
      return filtered;
    });
}

// Remove each item in the array that is not public
function filterItemsPublic(items){
  return DBpermissions
    .getAll().then(permissions => {
      var filtered = [];
      items.forEach(item => {
        const item_permissions = permissions[item.common.permissions_id-1];
        if(item_permissions.is_public){
          filtered.push(item);
        }
      });
      return filtered;
    });
}

module.exports = router;
