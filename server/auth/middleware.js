const User = require('../db/user');

function isLoggedIn(req, res, next){
  // verify there is a cookie
  if(req.signedCookies.user_id){
    // allow the next step
    next();
  }else{
    res.status(401);
    next(new Error('Un-Authorized'));
  }
}

/*function allowAccess(req, res, next){
  // confirm the user is allowed to access this ressource
  if(req.signedCookies.user_id === req.params.id){
    // allow the next step
    next();
  }else{
    res.status(401);
    next(new Error('Un-Authorized'));
  }
}*/

// confirm the user is an admin and admin access is allowed
function isAdmin(req, res, next){
  User
    .getOne(req.signedCookies.user_id)
    .then(user => {
      if(!user){
        //User in cookie does not exist
        res.status(401);
        next(new Error('Cookie does not match any existing user.'));
      }else{
        if(user.isAdmin){
          if(process.env.ALLOW_ADMIN_ACCESS === 'true'){
            next();
          }else{
            next(new Error('Admin access is disabled in the server ENV.'));
          }
        }else{
          res.status(401);
          next(new Error('Un-Authorized. Only admins can see this.'));
        }
      }
    });
}

module.exports = {
  isLoggedIn,
  //allowAccess,
  isAdmin
};
