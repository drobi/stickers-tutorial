// Update with your config settings.

require('dotenv').config();

module.exports = {

  development: {
    client: 'pg',
    connection: process.env.DEVELOPMENT_DATABASE_URL + '?ssl=true'
  },
  production: {
    client: 'pg',
    connection: process.env.PRODUCTION_DATABASE_URL + '?ssl=true'
  }

};