
exports.up = function(knex, Promise) {
  return knex.schema.createTable('item_common', table => {
    table.increments();
    table.datetime('created_at');
    table.text('manufacturer').notNullable();
    table.text('model').notNullable();
    table.integer('permissions_id').unique().notNullable();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('item_common');
};

