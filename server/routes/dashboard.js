const express = require('express');
const router = express.Router();
const User = require('../db/user');
//const authMiddleware = require('../auth/middleware');

// Returns the logged-in user info
router.get('/user', (req, res) => {
  User.getOne(req.signedCookies.user_id).then(user => {
    if (user) {
      delete user.password; //even if hashed, we don't want to send back the password
      res.json(user);
    } else {
      resError(res, 404, "User Not Found");
    }
  });
});

function resError(res, statusCode, message) {
  res.status(statusCode);
  res.json({message});
}

module.exports = router;
