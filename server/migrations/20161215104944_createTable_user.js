
exports.up = function(knex, Promise) {
  return knex.schema.createTable('user', table => {
    table.increments();
    table.text('email').unique().notNullable();
    table.text('password').notNullable();
    table.datetime('created_at');
    table.boolean('isAdmin').defaultTo(false);
    table.text('firstName').notNull();
    table.text('middleName');
    table.text('lastName').notNull();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('user');
};
