$(document).ready(function () {
  // make a request to the server for the user information
  getMyInfo()
    .then(addAdminInfoToPage)
    .catch(handleError);
  getTables()
    .then(showTables)
    .catch(handleError);

  //Raw SQL engine
  $('#btn-send-raw-sql').click(function () {
    let query = $('#input-sql-query').val();
    if(query === ''){
      query = "SELECT * FROM item_motor";
    }
    sendRawQuery(query)
      .then(showSQLresponse);
  });
});

function getTables() {
  return $.get(`${ADMIN_URL}/tables`);
}

function getMyInfo() {
  return $.get(`${API_URL}/dashboard/user`);
}

function sendRawQuery(query) {
  return $.post(`${ADMIN_URL}/db_raw`, {raw: query});
}

function showSQLresponse(res) {
  let source = $("#db-raw-response-template").html();
  let template = Handlebars.compile(source);
  let message = res.response;
  console.log(message);
  let context = message;
  let html = template(context);
  $('.db-raw-response').html(html);
  $('#json-render').append($(renderjson(message)));
}

function addAdminInfoToPage(user) {
  let source = $("#admin-template").html();
  let template = Handlebars.compile(source);
  let context = user;
  let html = template(context);
  $('.admin').html(html);
}

function showTables(res) {
  let source = $("#tables-template").html();
  let template = Handlebars.compile(source);
  let context = res;
  let html = template(context);
  $('.DBtables').html(html);
}

function handleError(error) {
  alert(error.responseJSON.message);
}