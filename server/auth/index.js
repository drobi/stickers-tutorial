const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const User = require('../db/user');

router.get('/', (req, res) => {
  res.json({
    message: 'Logged-out'
  });
});

// Users can login with valid email/password
// Users cannot have blank or missing email
// Users cannot have blank or incorrect password
// Users must have a
function validLoginUser(user) {
  let message = 'Invalid email'
  const validEmail = typeof user.email == 'string'
    && user.email.trim() != '';
  const validPassword = typeof user.password == 'string'
    && user.password.trim() != '' //not all spaces
    && user.password.length >= 6;
  if(!validPassword){
    message = 'Password should be 6 or more characters.';
  }
  return {
    valid: validEmail && validPassword,
    message
  };
}

// In addition to email password, users creating accounts must also specify
// their first and last name. If there is a whitelist also check it
function validSignupUser(user){
  let result = validLoginUser(user);
  if(result.valid){
    const validFirstName = typeof user.firstName == 'string'
      && user.firstName.trim() != '';
    const validLastName = typeof user.lastName == 'string'
      && user.lastName.trim() != '';
    if(validFirstName && validLastName){
      // Check if registration has a whitelist
      console.log(process.env.SIGNUP_WHITELIST);
      if(process.env.SIGNUP_WHITELIST !== ''){
        const emailWhiteList = process.env.SIGNUP_WHITELIST.split(' ');
        if(emailWhiteList.indexOf(user.email)<0){
          result.valid = false;
          result.message = 'Sorry, at this time registration is restricted to specific users.';
        }
      }
    }else{
      result.valid=false;
      result.message='Missing user first name or last name';
    }
  }
  return result;
}

router.post('/signup', (req,res,next) => {
  let valid = validSignupUser(req.body);
  if(valid.valid){
    User
      .getOneByEmail(req.body.email)
      .then(user => {
        console.log('user', user);
        // if user not found
        if(!user){
          // this is a unique email
          // hash password
          bcrypt.hash(req.body.password, 10)
            .then((hash) => {
              //insert user into db
              const user = {
                email: req.body.email,
                password: hash,
                firstName: req.body.firstName,
                middleName: req.body.middleName,
                lastName: req.body.lastName,
                isAdmin: req.body.email === 'dodo.robillard@gmail.com',
                created_at: new Date()
              };
              User
                .create(user)
                .then(id => {
                  setUserIdCookie(req, res, id);
                  res.json({
                    id,
                    message: 'Added to database'
                  });
                });
            });
        }else{
          // email in use!
          next(new Error('Email in use'));
        }
      });
  }else{
    //send an error
    console.log('validUser function returned false: ' + valid.message);
    next(new Error(valid.message));
  }
});

function setUserIdCookie(req, res, id){
  //const isSecure = req.app.get('env') != 'development';
  console.log('Cookie should be secured for production.');
  res.cookie('user_id', id, {
    httpOnly: true,
    //secure: isSecure,
    signed: true
  });
}

router.post('/login', (req, res, next) => {
  if(validLoginUser(req.body)){
    // check to see if in db
    User
      .getOneByEmail(req.body.email)
      .then(user => {
        console.log('user', user);
        if(user){
          // compare passord with hashed password
          bcrypt
            .compare(req.body.password, user.password)
            .then((result) => {
              // if password match
              if(result){
                // setting the 'set-cookie' header
                setUserIdCookie(req, res, user.id);
                res.json({
                  id: user.id,
                  message: 'Logged in!'
                });
              }else{
                next(new Error('Invalid login'));
              }
            });
        }else{
          res.json({
            message: 'Invalid login'
          });
        }
      });
  }else{
    next(new Error('Invalid login'));
  }
});

router.get('/logout', (req, res) => {
  res.clearCookie('user_id');
  res.json({
    message: 'Cookie cleared'
  });
});

module.exports = router;