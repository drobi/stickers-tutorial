# Sticker Mania

## ENVIRONMENT
This folder is for hosting on a static server.

## FIRST INIT
Just type:

```sh
npm install
```

## SERVER
Start the server by typing
...sh
npm start
...
You will be able to access the webserver by doing from the IDE: Preview running application.
When done you should type CTRL-C to stop the server and release the port.