/*
    table.increments();
    table.datetime('created_at');
    table.integer('common-id');
*/

exports.seed = (knex, Promise) => {
  return knex.raw('DELETE FROM item_motor; ALTER SEQUENCE item_motor_id_seq RESTART WITH 1')
    .then(() => {
      const item_motor = [{
        created_at: new Date(),
        item_common_id: 1
      },
      {
        created_at: new Date(),
        item_common_id: 2
      },
      {
        created_at: new Date(),
        item_common_id: 3
      },
      {
        created_at: new Date(),
        item_common_id: 4
      }
      ];

      return knex('item_motor').insert(item_motor);
    });
};