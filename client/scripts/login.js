redirectIfLoggedIn();

$('#login-form').submit((event) => {
  event.preventDefault();
  const email = $('#email').val();
  const password = $('#password').val();

  const user = {
    email,
    password
  };

  login(user)
    .then(applyLogin)
    .catch(error => {
      showError(error.responseJSON.message);
    });
});

function login(user) {
  return $.post(AUTH_URL + '/login', user);
}