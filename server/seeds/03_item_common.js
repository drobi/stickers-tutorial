/*
    table.increments();
    table.datetime('created_at');
    table.text('manufacturer').notNullable();
    table.text('model').notNullable();
    table.integer('permissions_id').unique().notNullable();
*/

exports.seed = (knex, Promise) => {
  return knex.raw('DELETE FROM item_common; ALTER SEQUENCE item_common_id_seq RESTART WITH 1')
    .then(() => {
      const item_common = [{
        created_at: new Date(),
        manufacturer: 'Tirnicrap',
        model: 'SpinfastX34 (admin-public)',
        permissions_id: 1
      },
      {
        created_at: new Date(),
        manufacturer: 'Tirnicrap',
        model: 'Spinslow RT (user-public)',
        permissions_id: 2
      },
      {
        created_at: new Date(),
        manufacturer: 'Custom',
        model: 'R&D1254 (admin-private)',
        permissions_id: 3
      },
      {
        created_at: new Date(),
        manufacturer: 'Crouppi',
        model: 'Rippa (admin-public)',
        permissions_id: 4
      }
      ];

      return knex('item_common').insert(item_common);
    });
};