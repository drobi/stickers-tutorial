$(document).ready(function () {
  // make a request to the server for the logged-in user information
  getUserInfo()
    .then(addUserInfoToPage)
    //.then(getStickers)
    //.then(addStickers)
    .catch(handleError);

  // make a request to the server for the list of user motors
  getUserMotors()
    .then(showMotorList)
    .catch(handleError);
});

function getUserInfo() {
  return $.get(`${API_URL}/dashboard/user`);
}

function getUserMotors() {
  return $.get(`${API_URL}/item/motor/user`);
}

function addUserInfoToPage(user) {
  let source = $("#user-template").html();
  let template = Handlebars.compile(source);
  let context = user;
  let html = template(context);
  $('.user').html(html);
  return true;
}

function showMotorList(motors) {
  let source = $("#motors-template").html();
  let template = Handlebars.compile(source);
  console.log(motors);
  let context = {motors};
  let html = template(context);
  $('.motors').html(html);
  return true;
}

function handleError(error) {
  alert(error);
}