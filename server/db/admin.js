const knex = require('./connection');

module.exports = {
  listTables: function () {
    return knex.raw("SELECT * FROM information_schema.tables WHERE table_schema = 'public'");
  },
  getTable: function (name) {
    return knex(name);
  },
  raw: function (req) {
    if(process.env.ALLOWADMINRAW === "true"){
      return knex.raw(req);
    }else{
      return Promise.resolve('For security reasons raw SQL requests are disabled on the server');
    }
  }
};