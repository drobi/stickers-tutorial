/*
    table.increments();
    table.datetime('created_at');
    table.integer('owner_id').notNullable();
*/

exports.seed = (knex, Promise) => {
  return knex.raw('DELETE FROM permissions; ALTER SEQUENCE permissions_id_seq RESTART WITH 1')
    .then(() => {
      const permissions = [{
        created_at: new Date(),
        owner_id: 2,
        is_public: true
      },
      {
        created_at: new Date(),
        owner_id: 1,
        is_public: true
      },
      {
        created_at: new Date(),
        owner_id: 2,
        is_public: false
      },
      {
        created_at: new Date(),
        owner_id: 2,
        is_public: true
      }
      ];

      return knex('permissions').insert(permissions);
    });
};