var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');

var index = require('./routes');
var dashboard = require('./routes/dashboard');
var item = require('./routes/item');
var auth = require('./auth');
var authMw = require('./auth/middleware');
var admin = require('./admin');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser(process.env.COOKIE_SECRET));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors({
  origin: process.env.ORIGIN_URL,
  credentials: true // needed to allow server to place cookies to remember login
}));

app.use('/admin', authMw.isLoggedIn, authMw.isAdmin, admin);
app.use('/auth', auth);
app.use('/', index);
app.use('/dashboard', authMw.isLoggedIn, dashboard);
app.use('/item', item);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: req.app.get('env') === 'development' ? err : {}
  });
});

module.exports = app;
