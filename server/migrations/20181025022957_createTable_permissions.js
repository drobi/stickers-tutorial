
exports.up = function(knex, Promise) {
  return knex.schema.createTable('permissions', table => {
    table.increments();
    table.datetime('created_at');
    table.integer('owner_id').notNullable();
    table.boolean('is_public');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('permissions');
};
