$(document).ready(function () {
  // get table name from url query
  const params = parseQuery();

  // make a request to the server for the user information
  getTableContent(params.name)
    .then(showTableContent)
    .catch(handleError);

  // show the table name
  let source = "<p>Table {{name}}:</p>";
  let template = Handlebars.compile(source);
  let html = template(params);
  $('.tableName').html(html);
});

function getTableContent(name) {
  const getRequest = ADMIN_URL + '/table/' + name;
  return $.get(getRequest);
}

function showTableContent(res) {
  let source = $("#tables-template").html();
  let template = Handlebars.compile(source);
  res.tableField = Object.keys(res.rows[0]);
  let context = res;
  let html = template(context);
  $('.tableList').html(html);
}

function handleError(error) {
  alert(error.responseJSON.message);
}