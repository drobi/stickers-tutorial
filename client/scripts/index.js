$(document).ready(function () {
    // make a request to the server for the list of motors
  getPublicMotors()
    .then(showMotorList)
    .catch(handleError);
  // make a request to the server for the logged-in user information
  getUserInfo()
    .then(showNavButtons)
    .catch(showNavButtons);
});

function getPublicMotors(id) {
  return $.get(`${API_URL}/item/motor/public`);
}

function showNavButtons(user){
  let source = $("#navigation-template").html();
  let template = Handlebars.compile(source);
  let context = user;
  let html = template(context);
  $('.navigation').html(html);
  return true;
}

function showMotorList(motors) {
  let source = $("#motors-template").html();
  let template = Handlebars.compile(source);
  console.log(motors);
  let context = {motors};
  let html = template(context);
  $('.motors').html(html);
  return true;
}

function getUserInfo() {
  return $.get(`${API_URL}/dashboard/user`);
}

function handleError(error) {
  alert(error);
}